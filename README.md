# Information / Информация

Интеграция системы CSS Grid.

## Syntax / Синтаксис

1. Загрузите папки и файлы в директорию `extensions/MW_EXT_Grid`.
2. В самый низ файла `LocalSettings.php` добавьте строку:

```php
wfLoadExtension( 'MW_EXT_Grid' );
```

## Install / Установка

```html
<grid>
<column>[CONTENT]</column>
<column>[CONTENT]</column>
<column>[CONTENT]</column>
</grid>
```

## Donations / Пожертвования

- [Donation Form](https://donation-form.github.io/)
